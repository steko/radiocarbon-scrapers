These scrapers, when ready, will make it possible to aggregate several
existing databases into one (giant) metadatabase of radiocarbon dates.

They're written in Javascript with Node.js and the scraperjs library.

You can do cool stuff with many thousand radiocarbon dates:

- [Reconstructing regional population fluctuations in the European Neolithic using radiocarbon dates: a new case-study using an improved method](http://dx.doi.org/10.1016/j.jas.2014.08.011)
