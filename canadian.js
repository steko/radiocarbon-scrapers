var async = require('async'),
    low = require('lowdb'),
    scraperjs = require('scraperjs');

low.load();

var scrapeBan = function(id, callback) {
    scraperjs.StaticScraper.create('http://www.canadianarchaeology.ca/localc14/detail.php?id=' + id)
	.scrape(function($) {
            return $("td").map(function() {
		return $(this).text();
            }).get().filter(function(labcode) {
		return labcode.lastIndexOf('\nLab No.:', 0) === 0;
	    })[0].trim().slice(9);
	}, function(labcode) {
	    //console.log(id);
	    low('c14').insert({source: 'Canadian Archaeological Radiocarbon Database', labcode: labcode});
	    callback(null, {
		labcode: labcode
	    });
	});
};

// example from caolan/async
// upper limit is 29027
async.times(25, function(n, next){
    scrapeBan(n, function(err, user) {
      next(err, user);
    });
}, function(err, users) {
    console.log(users);
});
