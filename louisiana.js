var async = require('async'),
    low = require('lowdb'),
    scraperjs = require('scraperjs'),
    router = new scraperjs.Router();

low.load();

var url = 'http://www.crt.state.la.us/dataprojects/archaeology/C14/index.asp',
    headers = {'Host': 'www.crt.state.la.us',
	       'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
	       'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0',
	       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
	       'Accept-Language': 'it-IT,it;q=0.8,en-US;q=0.5,en;q=0.3',
	       'Accept-Encoding': 'gzip, deflate',
	       'Referer': url,
	       'Connection': 'keep-alive'},
    body = 'searchtext=&searchterm=sitenumber&CRAsearchstyle=1&CRAuserRANGE1=&CRAuserRANGE2=&CRA=&CRA1=&CRA2=&CRAsearch=OFF&abvParish=NONE&CKParish=OFF&SEARCHTERMsearch=OFF&chkSortBy=sitenumber&TotalPages=85&AbsolutePage=1&PageSize=10';

scraperjs.StaticScraper.create()
    .request(url, method="POST", {body:body})
    .scrape(function($) {
        return $("input[name=Lab]").map(function() {
	    return $(this).attr("value");
        }).get();
    }, function(labcodes) {
	//labcodes.forEach(function(labcode) {
	    console.log(labcodes);
	//    low('c14').insert({source: 'Louisiana', labcode: labcode});
	//});
    });
