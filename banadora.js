var async = require('async'),
    low = require('lowdb'),
    scraperjs = require('scraperjs');

low.load();

var scrapeBan = function(id, callback) {
    scraperjs.StaticScraper.create('http://www.archeometrie.mom.fr/banadora/echantillon.php?num=' + id)
	.scrape(function($) {
            return $("tr").map(function() {
		return $(this).text();
            }).get().filter(function(labcode) {
		return labcode.lastIndexOf('  Code', 0) === 0;
	    })[0];
	}, function(labcode) {
	    // console.log(id);
	    low('c14').insert({source: 'BANADORA', labcode: labcode});
	    callback(null, {
		labcode: labcode
	    });
	});
};

// example from caolan/async
// upper limit is 39625
async.times(25, function(n, next){
    scrapeBan(n, function(err, user) {
      next(err, user);
    });
}, function(err, users) {
    console.log(users);
});
